package socketobjetostres;
import java.io.Serializable;

//Serializamos la clase (se tiene que serializar todos los objetos que van a pasar por el socket)
public class Mensaje implements Serializable {
    //creamos las variables
    private int id;
    private Hora hora;
    private NombreHost host;
    private String texto;

    //creamos el constructor
    public Mensaje(int id, String texto) {
        this.id = id;
        this.hora=new Hora();
        this.host=new NombreHost();
        this.texto = texto;
    }
    //generamos los get y set


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    @Override
    public String toString() {
        return
                host+ " | "+hora.toString()+ "M " + id + "--> " + texto ;
    }

    public static void main(String[] args) {
        Mensaje m =new Mensaje(0,"Hola soy un mensaje");
        System.out.println(m.toString());
    }
}
