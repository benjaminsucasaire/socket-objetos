package socketobjetostres;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;


//Serializamos la clase (se tiene que serializar todos los objetos que van a pasar por el socket)

public class Hora implements Serializable {
    //creamos 3 variables
    private int hora;
    private int minutos;
    private int segundos;

    //creamos la variable para el objeto calendar
    private Calendar calendar;

    //creamos constructor para recuperar los datos del sistema hora,minutos,segundos;
    public Hora(){
        //asignamos una estructura a la variable calendar
        calendar= GregorianCalendar.getInstance();
        // los datos seran almacenados en las variables creadas anteriormente
        this.hora=calendar.get(Calendar.HOUR);
        this.minutos=calendar.get(Calendar.MINUTE);
        this.segundos=calendar.get(Calendar.SECOND);
    }

    //generamos nuestros set y get

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    //insertamos un tostring, y lo modificamos

    @Override
    public String toString() {
        return hora + ":" + minutos + ":" + segundos +" | ";
    }

    public static void main(String[] args) {
        //instanciamos la clase hora, y se crea un objeto de tipo hora
       Hora hora= new Hora();
        System.out.println(hora.toString());
    }
}
