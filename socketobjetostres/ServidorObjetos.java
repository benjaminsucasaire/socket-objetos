package socketobjetostres;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorObjetos {
    //creamos las variables de tipo serversocket,socket,etc
    private ServerSocket servidor;
    private Socket socket;
    private int puerto;
    // objeto para enviar un mensaje
    private ObjectOutputStream emisor;
    //objeto para el receptor de mensaje
    private ObjectInputStream receptor;

        //creamos constructor
    public ServidorObjetos(int puerto){
            this.puerto=puerto;

            try {
                //inicializamos nuestro server socket que va tener como parametro un puerto
                this.servidor=new ServerSocket(puerto);
                //inicializamos nuestro socket
                this.socket=servidor.accept();//aceptar conexiones

                //creamos un emisor, el cual puede enviar un objeto
                emisor=new ObjectOutputStream(socket.getOutputStream());

                //creamos un receptor,el cual puede recibir un objeto
                receptor=new ObjectInputStream(socket.getInputStream());


            }catch (IOException ex){
                System.out.println("No se puedo conectar "+ex.getMessage());
            }
    }


    //funcion para enviar un mensaje, el cual va recivir como parametro un objeto
    public void enviar(Object mensaje){
        try {
            emisor.writeObject(mensaje);
        } catch (IOException e) {
            System.out.println("No se puedo enviar "+e.getMessage());
        }
    }
    //funcion para recibir un mensaje de tipo estring
    public Object recibir(){
        Object salida="";
        try {
            salida= receptor.readObject();
        } catch (Exception e) {
            System.out.println("No se puedo recibir "+e.getMessage());
        }
        return salida;
    }
    public static void main(String[] args) {
        ServidorObjetos s =new ServidorObjetos(5000);

        //el objeto ms es de tipo objeto, de la clase Mensaje
        Mensaje ms=new Mensaje(1,"Soy el servidor");
        s.enviar(ms);

        System.out.println(s.recibir());
    }

}
