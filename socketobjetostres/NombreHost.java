package socketobjetostres;


import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;


//Serializamos la clase (se tiene que serializar todos los objetos que van a pasar por el socket)
public class NombreHost implements Serializable {
    //creamos 3 variables
    private String host;
    //creamos la variable para el objeto calendar
    private InetAddress ia;

    //creamos constructor para recuperar los datos del sistema hora,minutos,segundos;
    public NombreHost(){
        //asignamos una estructura a la variable InetAddress
        try {
            ia = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        // los datos seran almacenados en las variables creadas anteriormente
        this.host=ia.getHostName();
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "Host:"+ host;
    }

    public static void main(String[] args) {
        //instanciamos la clase hora, y se crea un objeto de tipo hora
        NombreHost host= new NombreHost();
        System.out.println(host.toString());
    }
}
