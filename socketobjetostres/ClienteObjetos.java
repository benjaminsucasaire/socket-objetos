package socketobjetostres;

import java.io.*;
import java.net.Socket;

public class ClienteObjetos {
    //creamos la variable de tipo socket
    private Socket socket;
    //variables para la conexion con el servidor
    private String ip;
    private  int puerto;
    // objeto para enviar un mensaje
    private ObjectOutputStream emisor;
    //objeto para el receptor de mensaje
    private ObjectInputStream receptor;
    public ClienteObjetos(String ip, int puerto)  {
        this.ip = ip;
        this.puerto = puerto;
        try {
            //inicializamos nuestro server socket que va tener como parametro un puerto
            this.socket=new Socket(ip,puerto);//inicializamos nuestro socket
            //this.socket=socket.accept();//aceptar conexiones

            //creamos un receptor
            receptor=new ObjectInputStream(socket.getInputStream());

            //creamos un emisor
            emisor=new ObjectOutputStream(socket.getOutputStream());

        }catch (IOException ex){
            System.out.println("No se puedo conectar "+ex.getMessage());
        }
    }

    //funcion para enviar un mensaje
    public void enviar(Object mensaje){
        try {
            emisor.writeObject(mensaje);
        } catch (IOException e) {
            System.out.println("No se puedo enviar "+e.getMessage());
        }
    }

    //funcion para recibir un mensaje de tipo estring
    public Object recibir(){
        Object salida="";
        try {
            salida= receptor.readObject();
        } catch (Exception e) {
            System.out.println("No se puedo recibir "+e.getMessage());
        }
        return salida;
    }
    public static void main(String[] args) {
       ClienteObjetos c= new ClienteObjetos("localhost",5000);
        System.out.println(c.recibir());

        //el objeto ms es de tipo objeto, de la clase Mensaje
        Mensaje mc=new Mensaje(2,"Soy el cliente");
        c.enviar(mc);

    }
}
