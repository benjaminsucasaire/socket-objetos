package socketobjetostres;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Chat extends JFrame{
    private JButton servidorButton;
    private JButton clienteButton;
    private JTextArea textArea1;
    private JPanel mainPanel;
    private JTextField texfield;

    //creamos las variables
    private ServidorObjetos servidor;
    private ClienteObjetos cliente;

    private boolean soyServidor;
    private int contador;

    //funcion para crear hilo con su metodo run, la cual es independiente al codigo
    public void recibirMensajes(){
        //creamos hilo
      new Thread(new Runnable() {
          @Override
          public void run() {
            while (true){
                //condicional si es o no el servidor
                if (soyServidor){
                    // si soy servidor, recivir mensaje
                    Mensaje ms= (Mensaje) servidor.recibir();
                    textArea1.append("Amigo: "+ms.toString()+"\n");
                }else {
                    Mensaje ms=(Mensaje) cliente.recibir();
                    textArea1.append("Amigo: "+ms.toString()+"\n");
                }
            }
          }
      }).start();
    }
    //constructo
    public Chat(){

        contador=0;

        servidorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                servidor=new ServidorObjetos(5000);
                soyServidor=true;
                //llamamos a la funcion
                recibirMensajes();
            }
        });
        clienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                cliente=new ClienteObjetos("localhost",5000);
                soyServidor=false;
                //llamamos a la funcion
                recibirMensajes();
            }
        });

        //enviamos con los datos con solo aplastar enter
        texfield.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                super.keyPressed(keyEvent);
                //si aplasto enter
                if (keyEvent.getKeyCode()== keyEvent.VK_ENTER){
                    if (soyServidor){
                        Mensaje ms=new Mensaje(contador++,texfield.getText());
                        servidor.enviar(ms);
                        textArea1.append("yo: "+ms.toString()+"\n");
                        //funcion de limpieza
                        texfield.setText("");
                    }else {
                        Mensaje ms=new Mensaje(contador++,texfield.getText());
                        cliente.enviar(ms);
                        textArea1.append("yo: "+ms.toString()+"\n");
                        //funcion de limpieza
                        texfield.setText("");
                    }
                }
            }
        });
    }


    public static void main(String[] args) {
        Chat chat =new Chat();
        chat.setContentPane(new Chat().mainPanel);
        chat.setSize(750,700);
        chat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        chat.setVisible(true);
        chat.pack();

    }

}
