package socketbasic;

// ==========================================
// InetAddresDemo.java
// E-mail: contacto@javadesdecero.es
// Web:    https://javadesdecero.es
// ==========================================
import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddresDemo {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        try {
            // No hay un constructor definido en esta clase, el segundo es devolver tales instancias de objetos a través de métodos estáticos
            InetAddress ia = InetAddress.getLocalHost();


            System.out.println(ia.getHostName());

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}