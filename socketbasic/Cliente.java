package socketbasic;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Cliente {
    //creamos la variable de tipo socket
    private Socket socket;
    //variables para la conexion con el servidor
    private String ip;
    private  int puerto;
    // objeto para enviar un mensaje
    private DataOutputStream emisor;
    //objeto para el receptor de mensaje
    private DataInputStream receptor;
    public Cliente( String ip, int puerto)  {
        this.ip = ip;
        this.puerto = puerto;
        try {
            //inicializamos nuestro server socket que va tener como parametro un puerto
            this.socket=new Socket(ip,puerto);//inicializamos nuestro socket
            //this.socket=socket.accept();//aceptar conexiones

            //creamos un receptor
            receptor=new DataInputStream(socket.getInputStream());

            //creamos un emisor
            emisor=new DataOutputStream(socket.getOutputStream());

        }catch (IOException ex){
            System.out.println("No se puedo conectar "+ex.getMessage());
        }
    }

    //funcion para enviar un mensaje
    public void enviar(String mensaje){
        try {
            emisor.writeUTF(mensaje);
        } catch (IOException e) {
            System.out.println("No se puedo enviar "+e.getMessage());
        }
    }

    //funcion para recibir un mensaje de tipo estring
    public String recibir(){
        String salida="";
        try {
            salida= receptor.readUTF();
        } catch (IOException e) {
            System.out.println("No se puedo recibir "+e.getMessage());
        }
        return salida;
    }
    public static void main(String[] args) {
       Cliente c= new Cliente("localhost",5000);
        System.out.println(c.recibir());
        c.enviar("Soy el cliente");
    }
}
