package socketbasic;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    //creamos las variables de tipo serversocket,socket,etc
    private ServerSocket servidor;
    private Socket socket;
    private int puerto;
    // objeto para enviar un mensaje
    private DataOutputStream emisor;
    //objeto para el receptor de mensaje
    private DataInputStream receptor;

        //creamos constructor
    public Servidor(int puerto){
            this.puerto=puerto;

            try {
                //inicializamos nuestro server socket que va tener como parametro un puerto
                this.servidor=new ServerSocket(puerto);
                //inicializamos nuestro socket
                this.socket=servidor.accept();//aceptar conexiones

                //creamos un emisor
                emisor=new DataOutputStream(socket.getOutputStream());

                //creamos un receptor
                receptor=new DataInputStream(socket.getInputStream());


            }catch (IOException ex){
                System.out.println("No se puedo conectar "+ex.getMessage());
            }
    }


    //funcion para enviar un mensaje
    public void enviar(String mensaje){
        try {
            emisor.writeUTF(mensaje);
        } catch (IOException e) {
            System.out.println("No se puedo enviar "+e.getMessage());
        }
    }
    //funcion para recibir un mensaje de tipo estring
    public String recibir(){
        String salida="";
        try {
            salida= receptor.readUTF();
        } catch (IOException e) {
            System.out.println("No se puedo recibir "+e.getMessage());
        }
        return salida;
    }
    public static void main(String[] args) {
        Servidor s =new Servidor(5000);
        s.enviar("Soy el servidor");
        System.out.println(s.recibir());
    }

}
